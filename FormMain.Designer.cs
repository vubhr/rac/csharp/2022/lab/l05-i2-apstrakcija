﻿namespace L05_I2_Apstrakcija {
    partial class FormMain {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.gbKvadrat = new System.Windows.Forms.GroupBox();
            this.tbKvadratStranica = new System.Windows.Forms.TextBox();
            this.lblKvadratStranica = new System.Windows.Forms.Label();
            this.btnDodajKvadrat = new System.Windows.Forms.Button();
            this.tbPrikaz = new System.Windows.Forms.TextBox();
            this.gbKruznica = new System.Windows.Forms.GroupBox();
            this.btnDodajKruznicu = new System.Windows.Forms.Button();
            this.lblKruznicaRadijus = new System.Windows.Forms.Label();
            this.tbKruznicaRadijus = new System.Windows.Forms.TextBox();
            this.gbPravokutnik = new System.Windows.Forms.GroupBox();
            this.btnDodajPravokutnik = new System.Windows.Forms.Button();
            this.lblPravokutnikStranicaA = new System.Windows.Forms.Label();
            this.tbPravokutnikStranicaA = new System.Windows.Forms.TextBox();
            this.lblPravokutnikStranicaB = new System.Windows.Forms.Label();
            this.tbPravokutnikStranicaB = new System.Windows.Forms.TextBox();
            this.gbKvadrat.SuspendLayout();
            this.gbKruznica.SuspendLayout();
            this.gbPravokutnik.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbKvadrat
            // 
            this.gbKvadrat.Controls.Add(this.btnDodajKvadrat);
            this.gbKvadrat.Controls.Add(this.lblKvadratStranica);
            this.gbKvadrat.Controls.Add(this.tbKvadratStranica);
            this.gbKvadrat.Location = new System.Drawing.Point(12, 12);
            this.gbKvadrat.Name = "gbKvadrat";
            this.gbKvadrat.Size = new System.Drawing.Size(252, 173);
            this.gbKvadrat.TabIndex = 0;
            this.gbKvadrat.TabStop = false;
            this.gbKvadrat.Text = "Kvadrat:";
            // 
            // tbKvadratStranica
            // 
            this.tbKvadratStranica.Location = new System.Drawing.Point(19, 47);
            this.tbKvadratStranica.Name = "tbKvadratStranica";
            this.tbKvadratStranica.Size = new System.Drawing.Size(212, 23);
            this.tbKvadratStranica.TabIndex = 0;
            // 
            // lblKvadratStranica
            // 
            this.lblKvadratStranica.AutoSize = true;
            this.lblKvadratStranica.Location = new System.Drawing.Point(19, 29);
            this.lblKvadratStranica.Name = "lblKvadratStranica";
            this.lblKvadratStranica.Size = new System.Drawing.Size(52, 15);
            this.lblKvadratStranica.TabIndex = 1;
            this.lblKvadratStranica.Text = "Stranica:";
            // 
            // btnDodajKvadrat
            // 
            this.btnDodajKvadrat.Location = new System.Drawing.Point(19, 90);
            this.btnDodajKvadrat.Name = "btnDodajKvadrat";
            this.btnDodajKvadrat.Size = new System.Drawing.Size(212, 58);
            this.btnDodajKvadrat.TabIndex = 2;
            this.btnDodajKvadrat.Text = "Dodaj kvadrat";
            this.btnDodajKvadrat.UseVisualStyleBackColor = true;
            // 
            // tbPrikaz
            // 
            this.tbPrikaz.Location = new System.Drawing.Point(286, 22);
            this.tbPrikaz.Multiline = true;
            this.tbPrikaz.Name = "tbPrikaz";
            this.tbPrikaz.Size = new System.Drawing.Size(363, 598);
            this.tbPrikaz.TabIndex = 1;
            // 
            // gbKruznica
            // 
            this.gbKruznica.Controls.Add(this.btnDodajKruznicu);
            this.gbKruznica.Controls.Add(this.lblKruznicaRadijus);
            this.gbKruznica.Controls.Add(this.tbKruznicaRadijus);
            this.gbKruznica.Location = new System.Drawing.Point(12, 201);
            this.gbKruznica.Name = "gbKruznica";
            this.gbKruznica.Size = new System.Drawing.Size(252, 173);
            this.gbKruznica.TabIndex = 2;
            this.gbKruznica.TabStop = false;
            this.gbKruznica.Text = "Kružnica:";
            // 
            // btnDodajKruznicu
            // 
            this.btnDodajKruznicu.Location = new System.Drawing.Point(19, 90);
            this.btnDodajKruznicu.Name = "btnDodajKruznicu";
            this.btnDodajKruznicu.Size = new System.Drawing.Size(212, 58);
            this.btnDodajKruznicu.TabIndex = 2;
            this.btnDodajKruznicu.Text = "Dodaj kružnicu";
            this.btnDodajKruznicu.UseVisualStyleBackColor = true;
            // 
            // lblKruznicaRadijus
            // 
            this.lblKruznicaRadijus.AutoSize = true;
            this.lblKruznicaRadijus.Location = new System.Drawing.Point(19, 29);
            this.lblKruznicaRadijus.Name = "lblKruznicaRadijus";
            this.lblKruznicaRadijus.Size = new System.Drawing.Size(48, 15);
            this.lblKruznicaRadijus.TabIndex = 1;
            this.lblKruznicaRadijus.Text = "Radijus:";
            // 
            // tbKruznicaRadijus
            // 
            this.tbKruznicaRadijus.Location = new System.Drawing.Point(19, 47);
            this.tbKruznicaRadijus.Name = "tbKruznicaRadijus";
            this.tbKruznicaRadijus.Size = new System.Drawing.Size(212, 23);
            this.tbKruznicaRadijus.TabIndex = 0;
            // 
            // gbPravokutnik
            // 
            this.gbPravokutnik.Controls.Add(this.lblPravokutnikStranicaB);
            this.gbPravokutnik.Controls.Add(this.tbPravokutnikStranicaB);
            this.gbPravokutnik.Controls.Add(this.btnDodajPravokutnik);
            this.gbPravokutnik.Controls.Add(this.lblPravokutnikStranicaA);
            this.gbPravokutnik.Controls.Add(this.tbPravokutnikStranicaA);
            this.gbPravokutnik.Location = new System.Drawing.Point(12, 389);
            this.gbPravokutnik.Name = "gbPravokutnik";
            this.gbPravokutnik.Size = new System.Drawing.Size(252, 217);
            this.gbPravokutnik.TabIndex = 3;
            this.gbPravokutnik.TabStop = false;
            this.gbPravokutnik.Text = "Pravokutnik";
            // 
            // btnDodajPravokutnik
            // 
            this.btnDodajPravokutnik.Location = new System.Drawing.Point(19, 140);
            this.btnDodajPravokutnik.Name = "btnDodajPravokutnik";
            this.btnDodajPravokutnik.Size = new System.Drawing.Size(212, 58);
            this.btnDodajPravokutnik.TabIndex = 2;
            this.btnDodajPravokutnik.Text = "Dodaj pravokutnik";
            this.btnDodajPravokutnik.UseVisualStyleBackColor = true;
            // 
            // lblPravokutnikStranicaA
            // 
            this.lblPravokutnikStranicaA.AutoSize = true;
            this.lblPravokutnikStranicaA.Location = new System.Drawing.Point(19, 29);
            this.lblPravokutnikStranicaA.Name = "lblPravokutnikStranicaA";
            this.lblPravokutnikStranicaA.Size = new System.Drawing.Size(63, 15);
            this.lblPravokutnikStranicaA.TabIndex = 1;
            this.lblPravokutnikStranicaA.Text = "Stranica A:";
            // 
            // tbPravokutnikStranicaA
            // 
            this.tbPravokutnikStranicaA.Location = new System.Drawing.Point(19, 47);
            this.tbPravokutnikStranicaA.Name = "tbPravokutnikStranicaA";
            this.tbPravokutnikStranicaA.Size = new System.Drawing.Size(212, 23);
            this.tbPravokutnikStranicaA.TabIndex = 0;
            // 
            // lblPravokutnikStranicaB
            // 
            this.lblPravokutnikStranicaB.AutoSize = true;
            this.lblPravokutnikStranicaB.Location = new System.Drawing.Point(19, 73);
            this.lblPravokutnikStranicaB.Name = "lblPravokutnikStranicaB";
            this.lblPravokutnikStranicaB.Size = new System.Drawing.Size(62, 15);
            this.lblPravokutnikStranicaB.TabIndex = 4;
            this.lblPravokutnikStranicaB.Text = "Stranica B:";
            // 
            // tbPravokutnikStranicaB
            // 
            this.tbPravokutnikStranicaB.Location = new System.Drawing.Point(19, 91);
            this.tbPravokutnikStranicaB.Name = "tbPravokutnikStranicaB";
            this.tbPravokutnikStranicaB.Size = new System.Drawing.Size(212, 23);
            this.tbPravokutnikStranicaB.TabIndex = 3;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(679, 650);
            this.Controls.Add(this.gbPravokutnik);
            this.Controls.Add(this.gbKruznica);
            this.Controls.Add(this.tbPrikaz);
            this.Controls.Add(this.gbKvadrat);
            this.Name = "FormMain";
            this.Text = "Geometrijski oblici - apstrakcija";
            this.gbKvadrat.ResumeLayout(false);
            this.gbKvadrat.PerformLayout();
            this.gbKruznica.ResumeLayout(false);
            this.gbKruznica.PerformLayout();
            this.gbPravokutnik.ResumeLayout(false);
            this.gbPravokutnik.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GroupBox gbKvadrat;
        private Button btnDodajKvadrat;
        private Label lblKvadratStranica;
        private TextBox tbKvadratStranica;
        private TextBox tbPrikaz;
        private GroupBox gbKruznica;
        private Button btnDodajKruznicu;
        private Label lblKruznicaRadijus;
        private TextBox tbKruznicaRadijus;
        private GroupBox gbPravokutnik;
        private Label lblPravokutnikStranicaB;
        private TextBox tbPravokutnikStranicaB;
        private Button btnDodajPravokutnik;
        private Label lblPravokutnikStranicaA;
        private TextBox tbPravokutnikStranicaA;
    }
}